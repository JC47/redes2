#include "calculate.h"

int lastIndexOf(char cadena[], char needles[]){
	for (int i = strlen(cadena); i > -1; i--) {
		for (int j = 0; j < strlen(needles); j++) {
			if (cadena[i] == needles[j]) {
			return i;
			break;
		}
		}
	}
}


float calculate_prog_1(char *host, char op[30], CLIENT *clnt) {

	float  *result_1;
	inputs  solve_1_arg;

	char keys[] = "+-*/";
	int i = strcspn(op, keys);
	int j = strlen(op);
	int k = lastIndexOf(op, keys);

	if (i == 0) {
		printf("Pre \n");
		strcpy(solve_1_arg.expression, op);
		solve_1_arg.notation = 1;
		
		result_1 = solve_1(&solve_1_arg, clnt);
		if (result_1 == (float *)NULL) {
			clnt_perror(clnt, "call failed");
		}
		return *result_1;
	}
	else if (k == (j-1)) {
		printf("Pos \n");
		strcpy(solve_1_arg.expression, op);
		solve_1_arg.notation = 3;

		result_1 = solve_1(&solve_1_arg, clnt);
		if (result_1 == (float *)NULL) {
			clnt_perror(clnt, "call failed");
		}
		return *result_1;
	}
	else if (i != j || i == j) {
		printf("In \n");
		strcpy(solve_1_arg.expression, op);
		solve_1_arg.notation = 2;

		result_1 = solve_1(&solve_1_arg, clnt);
		if (result_1 == (float *)NULL) {
			clnt_perror(clnt, "call failed");
		}
		return *result_1;
	}
	else {
		printf("No hay operadores.\n");
		exit(0);
	}
}


int main (int argc, char *argv[]) {
	char *host;
	CLIENT *clnt;
	char exp[30];

	if (argc < 2) {
		printf ("usage: %s server_host\n", argv[0]);
		exit (1);
	}
	host = argv[1];
	clnt = clnt_create(host, CALCULATE_PROG, CALCULATE_VER, "udp");

	if (clnt == NULL) {
		clnt_pcreateerror(host);
		exit(1);
	}

	while (1) {
		printf("\nCalculadora distribuida con RPC\n");
		printf("Escriba la expresion:\n");
		scanf("%[^\n]", exp);
		printf("Resultado = %.3f\n", calculate_prog_1 (host,exp,clnt));
		printf("\n\n###################################################\n");
		getchar();
	}
	
	clnt_destroy(clnt);
	exit (0);
}

import java.net.*;
import java.io.*;

public class CPrimUDP {

    public static void main(String args[]) {
        try {
            String dst = "127.0.0.1";
            int pto = 2000;

            DatagramSocket cl = new DatagramSocket();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bos);

            dos.writeInt(4);
            dos.flush();
            dos.writeFloat(4.1f);
            dos.flush();
            dos.writeLong(72);
            dos.flush();

            byte[] b = bos.toByteArray();

            DatagramPacket p = new DatagramPacket(b, b.length, InetAddress.getByName(dst), pto);
            cl.send(p);
            cl.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
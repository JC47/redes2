import java.net.*;
import java.io.*;

public class SEcoTCPB{

    private static Socket cl;
    public static void main(String[] args){
        try {
            ServerSocket s = new ServerSocket(1234);
            System.out.println("Esperando cliente");

            while(true){
                cl = s.accept();
                System.out.println("Conexión aceptada desde " + cl.getInetAddress() + ": " + cl.getPort());

                BufferedWriter pw = new BufferedWriter(new OutputStreamWriter(cl.getOutputStream()));
                String msg1 = "Hola Mundo";
                pw.write(msg1);
                pw.flush();
                
                pw.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                cl.close();
            } catch (Exception e) {
            }
        }
    }
}
import java.net.*;
import java.io.*;

public class Cliente{
    public static void main(String args[]){
        ObjectInputStream ois = null;
        ObjectOutputStream oos = null;
        String host = "127.0.0.1";
        int port = 9999;

        try {
            Socket cl = new Socket(host,port);
            System.out.println("Conexion establecida");

            oos = new ObjectOutputStream(cl.getOutputStream());
            ois = new ObjectInputStream(cl.getInputStream());

            Usuario u = new Usuario("Javier","Calette","Cornelio","12345",20);

            System.out.println("Enviando objeto");
            oos.writeObject(u);
            oos.flush();
            
            System.out.println("Preparado para respuesta");

            Usuario u2 = (Usuario)ois.readObject();

            System.out.println("Recibo objeto");

            System.out.println(u2.getNombre());

        } catch (Exception e) {
            //TODO: handle exception
        }
    }
}
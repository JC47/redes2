import java.net.*;
import java.io.*;

public class CEcoUDPB{

    public static void main(String args[]){
        try {
            DatagramSocket cl = new DatagramSocket();
            String mensaje = "Perro enviado Perro enviado Perro enviado";
            byte[] b = mensaje.getBytes();
            String dst = "127.0.0.1";
            int pto = 2000;
            DatagramPacket p = new DatagramPacket(b,b.length,InetAddress.getByName(dst),pto);
            cl.send(p);

            DatagramPacket packet2 = new DatagramPacket(new byte[20], 20);
            cl.receive(packet2);
            System.out.println("Datagrama recibido desde " + packet2.getAddress());

            String msg = new String(packet2.getData(), 0, packet2.getLength());

            System.out.println("Mensaje recibido: " + msg);

            cl.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
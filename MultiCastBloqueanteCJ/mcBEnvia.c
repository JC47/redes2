#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>

#define MAX_LEN 1024
#define MIN_PORT 1024
#define MAX_PORT 65535

int main(int argc, char* argv[]){

    int sock;
    char send_str[MAX_LEN+1];
    struct sockaddr_in mc_addr;
    unsigned int send_len;
    char* mc_addr_str;
    unsigned short mc_port;
    unsigned char mc_ttl = 1;

    if(argc != 3){
        fprintf(stderr, "Uso %s <Multicast IP><Multicast puerto>\n", argv[0]);
        exit(1);
    }

    mc_addr_str = argv[1];
    mc_port = atoi(argv[2]);

    if((mc_port < MIN_PORT) || (mc_port > MAX_PORT)){
        printf(stderr, "NUmero de puerto invalid %d\n", mc_port);
        exit(1);
    }

    if( (sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0){
        perror("Error en la creación del socket");
        exit(1);
    }

    printf(" %d ", (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL, &mc_ttl, sizeof(mc_ttl))))
    if( (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL, &mc_ttl, sizeof(mc_ttl))) < 0){
        perror("Error en setsockopt");
        exit(1);
    }

    memset(&mc_addr, 0, sizeof(mc_addr));
    mc_addr.sin_family = AF_INET;
    mc_addr.sin_addr.s_addr = inet_addr(mc_addr_str);
    mc_addr.sin_port = htons(mc_port);

    printf("Comience a escribir (return para enviar ctrl-c para salir): \n");

    memset(send_str,0,sizeof(send_str));
    while(fgets(send_str,MAX_LEN,stdin)){
        send_len = strlen(send_str);
        
        if((sendto(sock, send_str, send_len,0, (struct sockaddr*)&mc_addr,sizeof(mc_addr))) != strlen){
            perror("Error en envio");
            exit(1);
        }

        memset(send_str,0,sizeof(send_str));
        
    }

    close(sock);
    exit(0);
}
import java.net.*;
import java.io.*;

public class MClient{
    public static void main(String args[]){

        

        try {
            MulticastSocket s = new MulticastSocket(10000);

            // Creamos el grupo multicast:
            InetAddress group = InetAddress.getByName("231.0.0.1");

            // Creamos un datagrama vacío en principio:
            s.joinGroup(group);
            byte[] vacio = new byte[0];
            DatagramPacket dgp = new DatagramPacket(vacio, 0, group, 10000);


            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String linea = br.readLine();

            // Creamos el buffer a enviar
            byte[] buffer = linea.getBytes();

            // Pasamos los datos al datagrama
            dgp.setData(buffer);

            // Establecemos la longitud
            dgp.setLength(buffer.length);

            // Y por último enviamos:
            s.send(dgp);

            linea = br.readLine();

            byte[] buffer2 = new byte[256];

            DatagramPacket dgp2 = new DatagramPacket(buffer2, buffer2.length);

            s.receive(dgp2);

            byte[] buffer3 = new byte[dgp2.getLength()];

            System.arraycopy(dgp2.getData(), 0, buffer3, 0, dgp2.getLength());

            String salida = new String(buffer3);
            System.out.println(salida);
        } catch (Exception e) {
            //TODO: handle exception
        }
        
    }

    

}
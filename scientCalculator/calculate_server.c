#include "calculate.h"

float evaluateIn(char exp[]) {
	char keys[] = "+-*/";
	int i = strcspn(exp, keys);
	int j = strlen(exp);
	if (i == j) {
		float res = atof(exp);
		if (res == 0) {
			if (strstr(exp, "pow")) {
				printf("Exponencial \n");
				int p1 = strcspn(exp, "(");
				int c = strcspn(exp, ",");
				int p2 = strcspn(exp, ")");
				char op1[10];
				char op2[10];
				substr(exp, op1, p1 + 1, c - p1 - 1);
				substr(exp, op2, c + 1, p2 - c - 1);
				float r1 = atof(op1);
				float r2 = atof(op2);
				return pow(r1,r2);
			}
			else {

				int a1 = strcspn(exp, "(");
				int a2 = strcspn(exp, ")");
				char trig[10];
				substr(exp,trig,a1+1,a2-a1-1);
				float trigOp = atof(trig);

				if (strstr(exp, "sen")) {
					printf("Seno \n");
					return sin((trigOp * 3.14159) / 180);
				}
				else if (strstr(exp, "cos")) {
					printf("Coseno \n");
					return cos((trigOp * 3.14159) / 180);
				}
				else if (strstr(exp, "tan")) {
					printf("Tangente \n");
					return tan((trigOp * 3.14159) / 180);
				}
				else if (strstr(exp, "log")) {
					printf("Logaritmo \n");
					return log10(trigOp);
				}
				else {
					return 0;
				}
			}
		}
		else {
			return res;
		}
	}
	else if (exp[i] == '+') {
		printf("Suma In \n");
		char op1[30];
		char op2[30];
		substr(exp, op1, 0, i);
		substr(exp, op2, i+1, j);
		return evaluateIn(op1) + evaluateIn(op2);
	}
	else if (exp[i] == '-') {
		printf("Resta In \n");
		char op1[30];
		char op2[30];
		substr(exp, op1, 0, i);
		substr(exp, op2, i + 1, j);
		return evaluateIn(op1) - evaluateIn(op2);
	}
	else if (exp[i] == '*') {
		printf("Mul In \n");
		char op1[30];
		char op2[30];
		substr(exp, op1, 0, i);
		substr(exp, op2, i + 1, j);
		return evaluateIn(op1) * evaluateIn(op2);
	}
	else if (exp[i] == '/'){
		printf("Div In \n");
		char op1[30];
		char op2[30];
		substr(exp, op1, 0, i);
		substr(exp, op2, i + 1, j);
		return evaluateIn(op1) / evaluateIn(op2);
	}
}

int lastIndexOf(char cadena[], char needles[]){
	for (int i = strlen(cadena); i > -1; i--) {
		for (int j = 0; j < strlen(needles); j++) {
			if (cadena[i] == needles[j]) {
			return i;
			break;
		}
		}
	}
}

void substr(char cadena[], char destino[], int comienzo, int longitud) {
    if (longitud == 0)
        longitud = strlen(cadena) - comienzo;

	destino[longitud] = '\0';
	strncpy(destino, cadena   + comienzo, longitud);
}

float evaluatePre(char exp[]) {
	char keys[] = "+-*/";
	int i = strcspn(exp, keys);
	// int j = strlen(exp);

	if (i == 0) {
		char subexp[30];
		substr(exp, subexp, strcspn(exp, " ")+1, strlen(exp));
		printf("Prueba1 %s\n", subexp);
		int aux = strcspn(subexp, " ");
		int aux2 = strlen(subexp);

		if (exp[i] == '+') {
			printf("Suma Pre\n");
			char op1[30];
			char op2[30];
			substr(subexp, op1, 0, aux);
			substr(subexp, op2, aux+1, aux2);
			printf("Prueba2- %s %s \n", op1, op2);
			return evaluatePre(op1) + evaluatePre(op2);
		}
		else if (exp[i] == '-') {
			printf("Resta Pre\n");
			char op1[30];
			char op2[30];
			substr(subexp, op1, 0, aux);
			substr(subexp, op2, aux + 1, aux2);
			return evaluatePre(op1) - evaluatePre(op2);
		}
		else if (exp[i] == '*') {
			printf("Mul Pre\n");
			char op1[30];
			char op2[30];
			substr(subexp, op1, 0, aux);
			substr(subexp, op2, aux + 1, aux2);
			return evaluatePre(op1) * evaluatePre(op2);
		}
		else if (exp[i] == '/'){
			printf("Div Pre\n");
			char op1[30];
			char op2[30];
			substr(subexp, op1, 0, aux);
			substr(subexp, op2, aux + 1, aux2);
			return evaluatePre(op1) / evaluatePre(op2);
		}
	}
	else {
		float res = atof(exp);
		if (res == 0) {
			if (strstr(exp, "pow")) {
				printf("Exponencial \n");
				int p1 = strcspn(exp, "(");
				int c = strcspn(exp, ",");
				int p2 = strcspn(exp, ")");
				char op1[10];
				char op2[10];
				substr(exp, op1, p1 + 1, c - p1 - 1);
				substr(exp, op2, c + 1, p2 - c - 1);
				float r1 = atof(op1);
				float r2 = atof(op2);
				return pow(r1,r2);
			}
			else {

				int a1 = strcspn(exp, "(");
				int a2 = strcspn(exp, ")");
				char trig[10];
				substr(exp,trig,a1+1,a2-a1-1);
				float trigOp = atof(trig);

				if (strstr(exp, "sen")) {
					printf("Seno \n");
					return sin((trigOp * 3.14159) / 180);
				}
				else if (strstr(exp, "cos")) {
					printf("Coseno \n");
					return cos((trigOp * 3.14159) / 180);
				}
				else if (strstr(exp, "tan")) {
					printf("Tangente \n");
					return tan((trigOp * 3.14159) / 180);
				}
				else if (strstr(exp, "log")) {
					printf("Logaritmo \n");
					return log10(trigOp);
				}
				else {
					return 0;
				}
			}
		}
		else {
			return res;
		}
	}
}

float evaluatePos(char exp[]) {
	char keys[] = "+-*/";
	int i = lastIndexOf(exp, keys);
	int j = strlen(exp);

	if (i == (j-1)) {
		char subexp[30];
		substr(exp, subexp, 0, lastIndexOf(exp, " "));
		printf("Prueba1 %s\n", subexp);
		int aux = lastIndexOf(subexp, " ");
		int aux2 = strlen(subexp);

		if (exp[i] == '+') {
			printf("Suma Pos\n");
			char op1[30];
			char op2[30];
			substr(subexp, op1, aux+1, (aux2-aux));
			substr(subexp, op2, 0, aux);
			return evaluatePos(op1) + evaluatePos(op2);
		}
		else if (exp[i] == '-') {
			printf("Resta Pos\n");
			char op1[30];
			char op2[30];
			substr(subexp, op1, aux+1, (aux2-aux));
			substr(subexp, op2, 0, aux);
			return evaluatePos(op1) - evaluatePos(op2);
		}
		else if (exp[i] == '*') {
			printf("Mul Pos\n");
			char op1[30];
			char op2[30];
			substr(subexp, op1, aux+1, (aux2-aux));
			substr(subexp, op2, 0, aux);
			return evaluatePos(op1) * evaluatePos(op2);
		}
		else if (exp[i] == '/'){
			printf("Div Pos\n");
			char op1[30];
			char op2[30];
			substr(subexp, op1, aux+1, (aux2-aux));
			substr(subexp, op2, 0, aux);
			return evaluatePos(op1) / evaluatePos(op2);
		}
	}
	else {
		float res = atof(exp);
		if (res == 0) {
			if (strstr(exp, "pow")) {
				printf("Exponencial \n");
				int p1 = strcspn(exp, "(");
				int c = strcspn(exp, ",");
				int p2 = strcspn(exp, ")");
				char op1[10];
				char op2[10];
				substr(exp, op1, p1 + 1, c - p1 - 1);
				substr(exp, op2, c + 1, p2 - c - 1);
				float r1 = atof(op1);
				float r2 = atof(op2);
				return pow(r1,r2);
			}
			else {

				int a1 = strcspn(exp, "(");
				int a2 = strcspn(exp, ")");
				char trig[10];
				substr(exp,trig,a1+1,a2-a1-1);
				float trigOp = atof(trig);

				if (strstr(exp, "sen")) {
					printf("Seno \n");
					return sin((trigOp * 3.14159) / 180);
				}
				else if (strstr(exp, "cos")) {
					printf("Coseno \n");
					return cos((trigOp * 3.14159) / 180);
				}
				else if (strstr(exp, "tan")) {
					printf("Tangente \n");
					return tan((trigOp * 3.14159) / 180);
				}
				else if (strstr(exp, "log")) {
					printf("Logaritmo \n");
					return log10(trigOp);
				}
				else {
					return 0;
				}
			}
		}
		else {
			return res;
		}
	}
}

float * solve_1_svc(inputs *argp, struct svc_req *rqstp) {
	static float  result;

	printf("Recibo: %s - %d\n", argp->expression, argp->notation);

	float aux = 0;

	switch (argp->notation) {
		case 1:
		{
			result = evaluatePre(argp->expression);
		}
		break;
		case 2:
		{
			result = evaluateIn(argp->expression);
		}
		break;
		case 3:
		{
			result = evaluatePos(argp->expression);
		}
		break;
	}

	return &result;
}
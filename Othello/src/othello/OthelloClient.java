
package othello;

import static java.lang.System.exit;
import javax.swing.JOptionPane;
import othello.Codes;

public class OthelloClient {
    
    public static void main(String[] args){
        //String dir = JOptionPane.showInputDialog("Ingrese la ip del servidor");
        SClient client = new SClient("127.0.0.1:3000");
        
        client.envia(Codes.JOIN+",");
        
        while(true){
            String[] in = client.recibe();
            if(in.length>0){
                System.out.println("Recibido :::" + in[0] + " - " + in[1]);
                int code = Integer.parseInt(in[0]);
                switch(code){
                    case Codes.JOINED:
                        JOptionPane.showMessageDialog(null, "Unido");
                        client.envia(Codes.LEAVE + ",Adios");
                        exit(0);
                    break;
                    case Codes.NOTJOINED:
                        JOptionPane.showMessageDialog(null, "Servidor ocupado,\nfavor conectarse a " + in[1]);
                        exit(0);
                    break;
                    case Codes.MOVE:
                    break;
                    case Codes.FINISHED:
                        JOptionPane.showMessageDialog(null, "El juego terminó");
                        exit(0);
                    break;
                }
            }
        }
    }
    
}

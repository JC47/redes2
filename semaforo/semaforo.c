#include<semaphore.h>
#include<pthread.h>
#include<stdio.h>
#include<unistd.h>

#define HILOS 20
sem_t okComprarLeche;
int lecheDisponible;

void *comprador(void *arg){
    int local;
    sem_wait(&okComprarLeche);
    if(lecheDisponible<5){
        local = lecheDisponible;
        local++;
        sleep(1);
        // ++lecheDisponible;
        lecheDisponible = local;
    }
    sem_post(&okComprarLeche);
}

int main(int argc, char* argv){
    pthread_t  hilos[HILOS];
    int i;
    lecheDisponible = 0;

    if(sem_init(&okComprarLeche,0,5)){
        printf("No se pudo incializar el semaforo");
        return -1;
    }

    for (i = 0; i < HILOS; i++)
    {
        if(pthread_create(&hilos[i],NULL,&comprador,NULL)){
            printf("Error al crear el hilo");
            return -1;
        }
    }

    for (i = 0; i < HILOS; i++)
    {
        pthread_join(hilos[i],NULL);
    }

    sem_destroy(&okComprarLeche);
    printf("Total de leche: %d \n", lecheDisponible);
    return 0;
}



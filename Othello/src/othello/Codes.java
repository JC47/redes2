
package othello;

public class Codes {
    public static final int JOIN=1;
    public static final int LEAVE=2;
    public static final int MOVE=3;
    public static final int JOINED=4;
    public static final int NOTJOINED=5;
    public static final int FINISHED=6;
    public static final int PORT=3000;
}

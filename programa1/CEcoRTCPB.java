import java.net.*;
import java.io.*;

public class CEcoRTCPB {
    private static Socket cl;

    public static void main(String[] args) {
        try {
            BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Escribe la dirección del servidor: ");
            String iserver = br1.readLine();
            System.out.println("\nEscribe el puerto del servidor: ");
            int pserver = Integer.parseInt(br1.readLine());
            cl = new Socket(iserver, pserver);
            br1.close();

            BufferedReader br2 = new BufferedReader(new InputStreamReader(cl.getInputStream()));
            PrintWriter pw = new PrintWriter(new OutputStreamWriter(cl.getOutputStream()));
            try {
                String msg = br2.readLine();
                System.out.println("Recibido: " + msg);
            } catch (Exception e) {
                //TODO: handle exception
            }
            
            try {
                String msg1 = "Hola servidor";
                pw.println(msg1);
                pw.flush();
            } catch (Exception e) {
                //TODO: handle exception
            }

            br2.close();
            pw.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                cl.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
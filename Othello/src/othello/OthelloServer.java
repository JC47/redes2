
package othello;

import java.net.*;
import othello.Codes;

public class OthelloServer {
        
    public static void main(String args[]){
        
        SServer server = new SServer();
        Partida partida1 =  new Partida();
        Partida partida2 = new Partida();
        
        while(true){
            String[] in = server.recibe();
            if(in.length>0){
                int code = Integer.parseInt(in[0]);
                switch(code){
                    case Codes.JOIN:
                        if(!partida1.fulled){
                            if(partida1.fulledP1()){
                                partida1.setUpP1(in[1]);
                                System.out.println("Partida1 P1 " + partida1.dirP1);
                            }
                            else{
                                partida1.setUpP2(in[1]);
                                System.out.println("Partida1 P2 " + partida1.dirP2);
                                partida1.fulled = true;
                            }
                            server.envia(Codes.JOINED+",", in[1]);
                        }
                        else if(!partida2.fulled){
                            if(partida2.fulledP1()){
                                partida2.setUpP1(in[1]);
                                System.out.println("Partida2 P1 " + partida2.dirP1);
                            }
                            else{
                                partida2.setUpP2(in[1]);
                                System.out.println("Partida2 P2 " + partida2.dirP2);
                                partida2.fulled = true;
                            }
                            server.envia(Codes.JOINED+",", in[1]);
                        }
                        else{
                            System.out.println("Not joined");
                            server.envia(Codes.NOTJOINED+",127.0.0.1:3001",in[1]);
                        }
                    break;
                    case Codes.LEAVE:
                        if(in[1].equals(partida1.dirP1)){
                            server.envia(Codes.FINISHED+",", partida1.dirP2);
                            partida1.finishMatch();
                        }
                        else if(in[1].equals(partida1.dirP2)){
                            server.envia(Codes.FINISHED+",", partida1.dirP1);
                            partida1.finishMatch();
                        }
                        else if(in[1].equals(partida2.dirP1)){
                            server.envia(Codes.FINISHED+",", partida2.dirP2);
                            partida2.finishMatch();
                        }
                        else if(in[1].equals(partida2.dirP2)){
                            server.envia(Codes.FINISHED+",", partida2.dirP1);
                            partida2.finishMatch();
                        }
                    break;
                    case Codes.MOVE:
                        System.out.println("Move");
                    break;
                }
            }
        }
    }
}
 
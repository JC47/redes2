import java.net.*;
import java.io.*;

public class SArchTCPB{
    public static void main(String[] args){
        try {
            ServerSocket s = new ServerSocket(7000);
            while(true){
                Socket cl = s.accept();

                System.out.println("Cliente "+cl.getInetAddress()+": "+ cl.getPort());
                DataInputStream dis = new DataInputStream(cl.getInputStream());
                String cuantos_aux = dis.readUTF();
                int cuantos = Integer.parseInt(cuantos_aux);
                for(int i=0;i<cuantos;i++){
                    byte[] b = new byte[1024];
                    String nombre = dis.readUTF();
                    System.out.println("Recibido " + nombre);

                    long tam = dis.readLong();

                    DataOutputStream dos = new DataOutputStream(new FileOutputStream(nombre));

                    long recibidos = 0;
                    int n, porcentaje;
                    while (recibidos < tam) {
                        if((recibidos+1024)>tam ){
                            int taux = (int) (tam - recibidos);
                            byte[] aux = new byte[taux];
                            n = dis.read(aux);
                            dos.write(aux, 0, n);
                        }
                        else{
                            n = dis.read(b);
                            dos.write(b, 0, n);
                        }
                        dos.flush();
                        recibidos = recibidos + n;
                        porcentaje = (int) (recibidos * 100 / tam);
                        System.out.println("Recibido: " + porcentaje + "%\r");
                    }
                    System.out.println("\n\n Archivo "+ nombre + " recibido\n");
                    dos.close();
                    
                }
                dis.close();
                cl.close();
            }
            
        } catch (Exception e) {
            //TODO: handle exception
        }
    }
}
import java.net.*;
import java.io.*;

public class SEcoUDPB{
    public static void main(String args[]){
        try {
            DatagramSocket s = new DatagramSocket(2000);
            while(true){
                DatagramPacket p = new DatagramPacket(new byte[20], 20);
                s.receive(p);
                System.out.println("Datagrama recibido desde "+ p.getAddress());

                String msg = new String(p.getData(),0,p.getLength());

                System.out.println("Mensaje recibido: " + msg);

                //Paquete de respuesta
                String res = "Perro respuesta";
                byte[] bres = res.getBytes();

                DatagramPacket respuesta = new DatagramPacket(bres, bres.length, p.getAddress(), p.getPort());

                s.send(respuesta);

                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
import java.net.*;

public class CMulticastB{
    public static void main(String[] args) {
        InetAddress gpo = null;
        try {
            MulticastSocket cl = new MulticastSocket(9999);
            System.out.println("Cliente escuchando puerto " + cl.getLocalPort());
            cl.reuseAddres(true);
            try {
                gpo = InetAddress.getByName("228.1.1.1");
            } catch (Exception e) {
                System.out.println("Dirección erronea");
            }
            cl.joinGroup(gpo);
            System.out.println("Unido al grupo");
            while(true){
                DatagramPacket p = new DatagramPacket(new byte[10], 10);
                cl.receive(p);
                String msj = new String(p.getData());
                System.out.println("Recibido: " + msj);
                System.out.println("Servidor descubireto: " + p.getAddress() + ": " + p.getPort());
            }
        } catch (Exception e) {
            //TODO: handle exception
        }
    }
}
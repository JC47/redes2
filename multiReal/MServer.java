import java.net.*;
import java.io.*;

public class MServer{

    public static void main(String args[]){
        try {
            MulticastSocket s = new MulticastSocket(10000);

            InetAddress group = null;
            try {
                group = InetAddress.getByName("231.0.0.1");
            } catch (UnknownHostException u) {
                System.out.println("Dirección erronea");
            }
            // Nos unimos al grupo:
            s.joinGroup(group);

            String salida = new String();

            while(true){

                byte[] buffer = new byte[256];

                DatagramPacket dgp = new DatagramPacket(buffer, buffer.length);

                s.receive(dgp);

                byte[] buffer2 = new byte[dgp.getLength()];

                System.arraycopy(dgp.getData(), 0, buffer2, 0, dgp.getLength());

                salida = new String(buffer2);
                System.out.println(salida);

                if(salida != ""){
                    byte[] vacio = new byte[0];
                    DatagramPacket dgpR = new DatagramPacket(vacio, 0, group, 10000);

                    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                    String linea = "Respuesta";

                    // Creamos el buffer a enviar
                    byte[] bufferR = linea.getBytes();

                    // Pasamos los datos al datagrama
                    dgpR.setData(bufferR);

                    // Establecemos la longitud
                    dgpR.setLength(bufferR.length);

                    // Y por último enviamos:
                    s.send(dgpR);

                    linea = br.readLine();
                }
            }
            
        } catch (Exception e) {
            //TODO: handle exception
        }
    }

}
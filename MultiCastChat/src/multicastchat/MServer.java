package multicastchat;

import java.util.ArrayList;
import java.util.function.Consumer;
import multicastchat.Codes;

public class MServer {
    
    public static void main(String args[]){
        
        GroupChat server = new GroupChat(Codes.SERVER);
        ArrayList<String> members = new ArrayList<>();
        
        while(true){
            String recibido = server.recibe();
            if(!recibido.isEmpty()){
                System.out.println(recibido);
                String[] msg = server.decodeMsg(recibido);
                int tipo = Integer.parseInt(msg[0]);
                switch(tipo){
                    case Codes.JOIN:
                        // Mensaje de bienvenida
                        String m1 = server.generateMsg(msg[1], Codes.DEFAULT, "Hola, te uniste al chat");
                        server.envia(m1);
                        // Mensaje de miebros
                        String preMemb = members.toString();
                        preMemb = preMemb.replace("[", "");
                        preMemb = preMemb.replace("]", "");
                        String memb = server.generateMsg(msg[1], Codes.MEMBERS, preMemb);
                        server.envia(memb);
                        // Notificación al resto
                        String m2 = server.generateMsg(Codes.ALL, Codes.JOIN, msg[1] + " se unió al chat");
                        server.envia(m2);
                        // Agrego a arraylist de miembros
                        members.add(msg[1]);
                    break;
                    case Codes.LEAVE:
                        String m3 = server.generateMsg(Codes.ALL, Codes.LEAVE, msg[1]+" abandonó el chat");
                        System.out.println(m3);
                        server.envia(m3);
                        members.remove(msg[1]);
                        System.out.println(members);
                    break;
                }
            }
        }
        
    }
}   

import javax.swing.JFileChooser;
import java.net.*;
import java.io.*;

public class CArchTCPB{
    public static void main(String[] args){
        try {
            Socket cl = new Socket("127.0.0.1",7000);   
            JFileChooser jt = new JFileChooser();
            jt.setMultiSelectionEnabled(true);
            int r = jt.showOpenDialog(null);
            if(r==JFileChooser.APPROVE_OPTION){
                File[] files = jt.getSelectedFiles();
                DataOutputStream dos = new DataOutputStream(cl.getOutputStream());
                System.out.println(files.length);
                dos.writeUTF(files.length+"");
                for(int i=0;i<files.length;i++){
                    String archivo = files[i].getAbsolutePath();
                    String nombre = files[i].getName();
                    long tam = files[i].length();
                    DataInputStream dis = new DataInputStream(new FileInputStream(archivo));
                    dos.writeUTF(nombre);
                    dos.flush();
                    dos.writeLong(tam);
                    byte[] b = new byte[1024];
                    long enviados = 0;
                    int porcentaje, n;
                    System.out.println("Enviando: " + nombre);
                    while (enviados < tam) {
                        n = dis.read(b);
                        dos.write(b, 0, n);
                        dos.flush();
                        enviados = enviados + n;
                        porcentaje = (int) (enviados * 100 / tam);
                        System.out.println("Enviando: " + porcentaje + "%\r");
                    }
                    System.out.println("\n\n Archivo "+ nombre +" enviado\n");
                    
                    dis.close();
                }
                dos.close();
                cl.close(); 
            }
        } catch (Exception e) {
            //TODO: handle exception
        }
    }
}
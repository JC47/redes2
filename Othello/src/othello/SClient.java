
package othello;

import java.net.*;

public class SClient {
    
    DatagramSocket cl;
    int port;
    InetAddress serverAddr;
    
    public SClient(String dir){
        try{
            this.cl = new DatagramSocket();
            this.serverAddr = InetAddress.getByName(dir.substring(0,dir.indexOf(':')));
            this.port = Integer.parseInt(dir.substring((dir.indexOf(':')+1)));
        }
        catch(Exception e){
            System.out.println("No se pudo conectar");
            System.out.println(e.toString());
        }
    }
    
    public boolean envia(String msg){
        boolean p = false;
        try{
                        
            byte[] bufferR = msg.getBytes();
            
            DatagramPacket dgpR = new DatagramPacket(bufferR, bufferR.length, this.serverAddr, this.port);
            
            this.cl.send(dgpR);
            
            p = true;
        }
        catch(Exception e){
            System.out.println("No se pudo enviar");
            System.out.println(e.toString());
        }
        return p;
    }
    
    public String[] recibe(){
        String[] entrada = new String[2];
        try{
            byte[] buffer = new byte[256];

            DatagramPacket dgp = new DatagramPacket(buffer, buffer.length);

            this.cl.receive(dgp);

            byte[] buffer2 = new byte[dgp.getLength()];

            System.arraycopy(dgp.getData(), 0, buffer2, 0, dgp.getLength());
            
            String str_in = new String(buffer2);
            
            entrada[0] = str_in.substring(0, str_in.indexOf(','));
            entrada[1] = str_in.substring((str_in.indexOf(',')+1));
        }
        catch(Exception e){
            System.out.println("No se pudo recibir");
            System.out.println(e.toString());
        }
        return entrada;
    }
    
    
}

struct inputs {
    char expression[30];
    int notation;
};

program CALCULATE_PROG{
    version CALCULATE_VER{
        float solve(inputs)=1;
    }=1;
}=0x2fffffff;
import java.net.*;
import java.io.*;

public class Servidor{
    public static void main(String args[]){
        ObjectInputStream ois = null;
        ObjectOutputStream oos = null;

        try {
            ServerSocket s = new ServerSocket(9999);
            System.out.println("Servidor iniciado");
            while(true){
                Socket cl = s.accept();
                System.out.println("Cliente conectado");

                oos = new ObjectOutputStream(cl.getOutputStream());
                ois = new ObjectInputStream(cl.getInputStream());

                Usuario u = (Usuario)ois.readObject();

                System.out.println("Objeto recibido");
                System.out.println(u.getApaterno());

                System.out.println("Devolviendo objeto");
                oos.writeObject(u);
                oos.flush();
            }
        } catch (Exception e) {
            //TODO: handle exception
        }
    }
}
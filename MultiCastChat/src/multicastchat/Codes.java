
package multicastchat;


public class Codes {
    public static final int JOIN=1;
    public static final int DEFAULT=2;
    public static final int LEAVE=3;
    public static final int MEMBERS=4;
    public static final String SERVER="Main";
    public static final String ALL = "All";
}

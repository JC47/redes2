#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>

#define MAX_LEN 1024
#define MIN_PORT 1024
#define MAX_PORT 65535

int main(int argc, char* argv[]){
    int sock;
    int flag_on = 1;
    struct sockaddr_in mc_addr;
    char recv_str[MAX_LEN+1];
    int recv_len;
    struct ip_mreq mc_req;
    char *mc_addr_str;
    unsigned short mc_port;
    struct sockaddr_in from_addr;
    unsigned int from_len;

    if(argc != 3){
        fprintf(stderr, "Uso %s <Multicast IP><Multicast puerto>\n", argv[0]);
        exit(1);
    }

    mc_addr_str = argv[1];
    mc_port = atoi(argv[2]);

    if((mc_port < MIN_PORT) || (mc_port > MAX_PORT)){
        printf(stderr, "NUmero de puerto invalid %d\n", mc_port);
        exit(1);
    }

    if( (sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0){
        perror("Error en la creación del socket");
        exit(1);
    }

    if( (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &flag_on, sizeof(flag_on))) < 0){
        perror("Error en setsockopt");
        exit(1);
    }

    memset(&mc_addr,0,sizeof(mc_addr));
    mc_addr.sin_family = AF_INET;
    mc_addr.sin_addr.s_addr=htonl(INADDR_ANY);
    mc_addr.sin_port = htons(mc_port);

    if( (bind(sock, (struct sockaddr*)&mc_addr,sizeof(mc_addr))) < 0 ){
        perror("Error en bind");
        exit(1);
    }

    mc_req.imr_multiaddr.s_addr = inet_addr(mc_addr_str);
    mc_req.imr_interface.s_addr = htonl(INADDR_ANY);

    if( (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void*)&mc_req, sizeof(mc_req))) < 0 ){
        perror("Error en membership");
        exit(1);
    }

    for(;;){
        memset(recv_str,0,sizeof(recv_str));
        from_len = sizeof(from_addr);
        memset(&from_addr,0,from_len);

        if( (recv_len = recvfrom(sock, recv_str, MAX_LEN,0, (struct sockaddr*)&from_addr,&from_addr)) < 0 ){
            perror("Error al recibir el paquete");
            exit(1);
        }

        printf("\nSe recibieron %d bytes desde %s", recv_len, inet_ntoa(from_addr.sin_addr));
        printf("\n %s \n", recv_str);

        if( setsockopt(sock, IPPROTO_IP, IP_DROP_MEMBERSHIP, (void*)&mc_req, sizeof(mc_req)) ){
            perror("Error en sockopt al drop membarship");
            exit(1);
        }

        close(sock);
    }
}
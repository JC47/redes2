
package multicastchat;

import java.io.IOException;
import javax.swing.JOptionPane;
import javax.swing.text.BadLocationException;

public class MultiCastChat {

    /**
     * @param args the command line arguments
     * @throws javax.swing.text.BadLocationException
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws BadLocationException, IOException {
                
        String name = JOptionPane.showInputDialog("Ingrese su nombre");
        
        ClientChat face = new ClientChat(name);
        
        face.setVisible(true);
        
        while(true){
            String aux = face.client.recibe();
            if(!aux.isEmpty()){
                System.out.println("Se recibió::: " +aux);
                face.messageReceived(aux);
            }
        }
        
    }
    
}

package othello;


public class Partida {
    
    
    String dirP1;
    String dirP2;
    boolean finished;
    boolean fulled;

    public Partida() {
        this.dirP1 = new String();
        this.dirP2 = new String();
        this.finished = false;
        this.fulled = false;
    }

    public boolean isFulled() {
        return fulled;
    }

    public void setFulled(boolean fulled) {
        this.fulled = fulled;
    }

    public void setUpP1(String n1){
        this.dirP1 = n1;
    }
    
    public void setUpP2(String n1){
        this.dirP2 = n1;
    }
    
    public void finishMatch() {
        this.fulled = false;
        this.dirP1 = "";
        this.dirP2 = "";
    }
    
    public boolean fulledP1(){
        return this.dirP1.isEmpty();
    }
    
    public boolean fulledP2(){
        return this.dirP2.isEmpty();
    }
    
    
}

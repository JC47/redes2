import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;

public class SEcoTCPNB{

    public static void main(String args[]) {
        try {
            String EEco = "";
            int pto = 9999;
            ServerSocketChannel s = ServerSocketChannel.open();
            s.configureBlocking(false);
            s.socket().bind(new InetSocketAddress(pto));
            System.out.println("Esperando cliente...");
            Selector sel = Selector.open();
            s.register(sel, SelectionKey.OP_ACCEPT);

            while(true){
                sel.select();

                Iterator <SelectionKey> it = sel.selectedKeys().iterator();

                while(it.hasNext()){
                    SelectionKey k = (SelectionKey) it.next();
                    it.remove();
                    if(k.isAcceptable()){
                        SocketChannel cl = s.accept();

                        System.out.println("Cliente conectado");

                        cl.configureBlocking(false);

                        cl.register(sel, SelectionKey.OP_READ | SelectionKey.OP_WRITE);

                        continue;
                    }

                    if(k.isReadable()){
                        try {
                            SocketChannel ch = (SocketChannel) k.channel();

                            ByteBuffer b = ByteBuffer.allocate(2000);
                            b.clear();
                            int n = 0;
                            String msg = "";
                            n = ch.read(b);
                            b.flip();
                            if(n > 0){
                                msg = new String(b.array(), 0, n);
                            }
                            System.out.println("Mensaje recibido "+ msg);

                            if(msg.equalsIgnoreCase("SALIR")){
                                k.interestOps(SelectionKey.OP_WRITE);
                                ch.close();
                            }
                            else{
                                EEco = "Eco ->" + msg;
                                k.interestOps(SelectionKey.OP_WRITE);
                            }
                        } catch (Exception e) {
                           continue;
                        }
                    }/* else if(k.isWritable()){
                        try {
                            SocketChannel ch = (SocketChannel) k.channel();
                            ByteBuffer bb = ByteBuffer.wrap(EEco.getBytes());
                            ch.write(bb);

                            System.out.println("Enviado " + EEco);

                            EEco = "";
                        } catch (Exception e) {
                            k.interestOps(SelectionKey.OP_READ);
                            continue;
                        }
                    } */
                }
            }
        } catch (Exception e) {
            //TODO: handle exception
        }
    }

}


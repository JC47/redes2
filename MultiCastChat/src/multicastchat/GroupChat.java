
package multicastchat;

import java.io.*;
import java.net.*;

public class GroupChat {
    MulticastSocket s;
    InetAddress group;
    int port = 10000;
    String name;
    
    public GroupChat(String n){
        this.name = n;
        try{ 
            this.s = new MulticastSocket(this.port);
            this.group = InetAddress.getByName("231.0.0.1");
            
            s.joinGroup(group);
            
            System.out.println(this.name + " Unido al grupo");
        }
        catch(IOException e){
            System.out.println("Algo salió mal");
            System.out.println(e.toString());
        }
    }
    
    public String recibe(){
        String entrada = new String();
        try{
            byte[] buffer = new byte[256];

            DatagramPacket dgp = new DatagramPacket(buffer, buffer.length);

            s.receive(dgp);

            byte[] buffer2 = new byte[dgp.getLength()];

            System.arraycopy(dgp.getData(), 0, buffer2, 0, dgp.getLength());
            
            String str_in = new String(buffer2);
            
            String receivers = str_in.substring((str_in.indexOf('<')+1), str_in.indexOf('>'));
            String msg = str_in.substring((str_in.indexOf('>')+1));
            String sender = str_in.substring((str_in.indexOf("*")+1), str_in.lastIndexOf("*"));
            int type = Integer.parseInt(str_in.substring((str_in.indexOf("-")+1), str_in.lastIndexOf("-")));
             
            if(receivers.contains(this.name)){
                entrada = type+":"+sender+":"+msg;
            }
            else if(receivers.contains(Codes.ALL) && type==Codes.JOIN && !this.name.equals(sender)){
                if(!msg.contains(this.name)){
                    entrada = type+":"+sender+":"+msg;
                }
            }
            else if(receivers.contains(Codes.ALL) && !this.name.equals(sender) && (type==Codes.DEFAULT || type==Codes.LEAVE)){
                entrada = type+":"+sender+":"+msg;
            }
        }
        catch(IOException e){
            //System.out.println("No se pudo recibir");
            //System.out.println(e.toString());
        }
        return entrada;
    }
    
    public String generateMsg(String receiver, int type, String body){
        return "*"+this.name+"*-"+type+"-"+"<"+receiver+">"+body;
    }
    
    public String[] decodeMsg(String msg){
        String type = msg.substring(0,msg.indexOf(":"));
        String sender = msg.substring((msg.indexOf(":")+1),msg.lastIndexOf(":"));
        String body = msg.substring((msg.lastIndexOf(":")+1));
        String[] p = {type,sender,body};
        return p;
    }
    
    public boolean envia(String msg){
        boolean p = false;
        try{
            
            byte[] bufferR = msg.getBytes();
            
            DatagramPacket dgpR = new DatagramPacket(bufferR, bufferR.length, this.group, this.port);
            
            s.send(dgpR);
            
            p = true;
        }
        catch(IOException e){
            System.out.println("No se pudo enviar");
            System.out.println(e.toString());
        }
        return p;
    }
    
    public void close(){
        this.s.close();
    }
}

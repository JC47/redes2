import java.net.*;
import java.io.*;

public class CEcoTCPB {
    private static Socket cl;
    public static void main(String[] args) {
        try {
            BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Escribe la dirección del servidor: ");
            String iserver = br1.readLine();
            System.out.println("\nEscribe el puerto del servidor: ");
            int pserver = Integer.parseInt(br1.readLine());
            cl = new Socket(iserver,pserver);
            br1.close();

            
            BufferedReader br2 = new BufferedReader(new InputStreamReader(cl.getInputStream()));
            String msg = br2.readLine();
            System.out.println("Recibido: " + msg);

            br2.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                cl.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
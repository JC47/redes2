#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <malloc.h>
#include <unistd.h>

#define DATOS 3500
#define R_BASE 1000

int *desorden;
int *orden;

pthread_mutex_t aux_block;

struct bucket {
    int orden;
    int rango1;
    int rango2;
};

struct response {
    int orden;
    int* body;
};

void* ordena(void *arg){
    pthread_mutex_lock(&aux_block);
    struct bucket *datos = (struct bucket *)arg;
    printf("Cubeta: %d, menor: %d, mayor: %d\n", datos->orden, datos->rango1, datos->rango2);
    int cuantos=0;
    for(int i = 0; i < DATOS; i++){
        if((desorden[i]>=datos->rango1)&&(desorden[i]<=datos->rango2)){
            cuantos++;
        }
    }
    int *orden_aux = (int*)malloc(sizeof(int)*cuantos);
    int cont = 0;
    for(int i = 0; i < DATOS; i++){
        if((desorden[i]>=datos->rango1)&&(desorden[i]<=datos->rango2)){
            orden_aux[cont]=desorden[i];
            cont++;
        }
    }
    int i1,i2,temp;
    for(i1 = 0; i1 < (cuantos-1); i1++){
        for(i2=i1+1; i2 < cuantos; i2++){
            if(orden_aux[i2]<orden_aux[i1]){
                temp=orden_aux[i2];
                orden_aux[i2] = orden_aux[i1];
                orden_aux[i1] = temp;
            }
        }
    }
    printf("Cuantos: %d\n",cuantos);
    struct response *res = malloc(sizeof(struct response));
    res->orden=datos->orden;
    res->body=orden_aux;
    pthread_mutex_unlock(&aux_block);
    return (void *)res;
}

int main(int argc, char* argv[]){

    // Datos base
    FILE *origen;
    srand(time(NULL));
    desorden = (int *)malloc(DATOS * sizeof(int));
    pthread_mutex_init(&aux_block, NULL);

    // Valido que venga el parametro
    if(argv[1] == NULL){
        printf("\nNo viene el parametro\n\n");
        exit(-1);
    }

    // Se obtiene el numero de cubetas
    int buckets = atoi(argv[1]);

    // Array de structuras
    struct bucket *cubetas[buckets];
    pthread_t threads[buckets];

    // Se valida el número de ingresado
    if(buckets<1 || buckets>1000){
        printf("\nNúmero de cubetas invalido\n\n");
        exit(-1);
    }

    // Despliego el numero de cubetas
    printf("El número de cubetas es: %d \n", buckets);

    // Se abre el archivo
    origen = fopen("desorden.txt","wt");
    if (origen == NULL) {
        fputs("File error", stderr);
        exit(1);
    }

    // Se llena el arrreglo en desorden y escribo el archivo
    for(int i = 0;i<DATOS;i++){
        int n =  rand()%1000;
        desorden[i]=n;
        fprintf(origen,"%d\n",n);
    }
    fclose(origen);

    int rango = (double)R_BASE/(double)buckets;

    printf("Rango: %d \n", rango);

    for(int i=0;i<buckets;i++){
        cubetas[i] = (struct bucket *)malloc(sizeof(struct bucket));
        cubetas[i]->orden = i;
        cubetas[i]->rango1 = i*rango;
        cubetas[i]->rango2 = ((i + 1) * rango) - 1;
        if((i==(buckets-1)) && cubetas[i]->rango2 < 999){
            cubetas[i]->rango2 = (999 - (cubetas[i]->rango1)) + cubetas[i]->rango1;
        }

        if(pthread_create(&threads[i], NULL, &ordena, cubetas[i])){
            printf("Error al crear el hilo");
            return -1;
        }
    }
    struct response *aux[buckets];
    for (int i = 0; i < buckets; i++) {
        aux[i]=(struct response *)malloc(sizeof(struct response));
        pthread_join(threads[i], (void *)&aux[i]);
    }

    for(int i=0; i<buckets;i++){
        printf("----------------- R Orden: %d -----------------\n", aux[i]->orden);
        int j = 0;
        while(aux[i]->body[j]!=NULL){
            printf("R Body: %d\n", aux[i]->body[j]);
            j++;
        }
    }

    free(desorden);
    pthread_mutex_destroy(&aux_block);
    return 0;
}
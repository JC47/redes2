/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#ifndef _CALCULATE_H_RPCGEN
#define _CALCULATE_H_RPCGEN

#include <rpc/rpc.h>


#ifdef __cplusplus
extern "C" {
#endif


struct inputs {
	char expression[30];
	int notation;
};
typedef struct inputs inputs;

#define CALCULATE_PROG 0x2fffffff
#define CALCULATE_VER 1

#if defined(__STDC__) || defined(__cplusplus)
#define solve 1
extern  float * solve_1(inputs *, CLIENT *);
extern  float * solve_1_svc(inputs *, struct svc_req *);
extern int calculate_prog_1_freeresult (SVCXPRT *, xdrproc_t, caddr_t);

#else /* K&R C */
#define solve 1
extern  float * solve_1();
extern  float * solve_1_svc();
extern int calculate_prog_1_freeresult ();
#endif /* K&R C */

/* the xdr functions */

#if defined(__STDC__) || defined(__cplusplus)
extern  bool_t xdr_inputs (XDR *, inputs*);

#else /* K&R C */
extern bool_t xdr_inputs ();

#endif /* K&R C */

#ifdef __cplusplus
}
#endif

#endif /* !_CALCULATE_H_RPCGEN */

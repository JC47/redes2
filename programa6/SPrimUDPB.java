import java.net.*;
import java.io.*;

public class SPrimUDPB{

    public static void main(String args[]){
        try {
            DatagramSocket s = new DatagramSocket(2000);
            while (true) {
                DatagramPacket p = new DatagramPacket(new byte[2000], 2000);
                s.receive(p);
                System.out.println("Datagrama recibido desde " + p.getAddress());

                DataInputStream dis = new DataInputStream(new ByteArrayInputStream(p.getData()));

                int x = dis.readInt();
                float f = dis.readFloat();
                long l = dis.readLong();

                System.out.println("Int " + x + " Float " + f + " Long " + l);
            }
        } catch (Exception e) {
            //TODO: handle exception
        }
    }
}
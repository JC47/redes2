package othello;

import java.io.*;
import java.net.*;

public class SServer {
    
    DatagramSocket s;
    
    public SServer(){
        try{
            this.s = new DatagramSocket(Codes.PORT);
            System.out.println("Server ready");
        }
        catch(Exception e){
        }
    }
    
    public boolean envia(String msg, String dir){
        boolean p = false;
        try{
            
            int port = Integer.parseInt(dir.substring((dir.indexOf(':')+1)));
            
            byte[] bufferR = msg.getBytes();
            
            DatagramPacket dgpR = new DatagramPacket(bufferR, bufferR.length, InetAddress.getByName(dir.substring(0, dir.indexOf(':'))), port);
            
            s.send(dgpR);
            
            p = true;
        }
        catch(IOException e){
            System.out.println("No se pudo enviar");
            System.out.println(e.toString());
        }
        return p;
    }
    
    public String[] recibe(){
        String[] entrada = new String[3];
        try{
            byte[] buffer = new byte[256];

            DatagramPacket dgp = new DatagramPacket(buffer, buffer.length);

            this.s.receive(dgp);

            byte[] buffer2 = new byte[dgp.getLength()];

            System.arraycopy(dgp.getData(), 0, buffer2, 0, dgp.getLength());
            
            String str_in = new String(buffer2);
            
            entrada[0] = str_in.substring(0, str_in.indexOf(','));
            entrada[1] = dgp.getAddress().toString().substring(1)+ ":" + dgp.getPort();
            entrada[2] = str_in.substring((str_in.indexOf(',')+1));
            
            /*String receivers = str_in.substring((str_in.indexOf('<')+1), str_in.indexOf('>'));
            String msg = str_in.substring((str_in.indexOf('>')+1));
            String sender = str_in.substring((str_in.indexOf("*")+1), str_in.lastIndexOf("*"));
            int type = Integer.parseInt(str_in.substring((str_in.indexOf("-")+1), str_in.lastIndexOf("-")));
             
            if(receivers.contains(this.name)){
                entrada = type+":"+sender+":"+msg;
            }
            else if(receivers.contains(Codes.ALL) && type==Codes.JOIN && !this.name.equals(sender)){
                if(!msg.contains(this.name)){
                    entrada = type+":"+sender+":"+msg;
                }
            }
            else if(receivers.contains(Codes.ALL) && !this.name.equals(sender) && (type==Codes.DEFAULT || type==Codes.LEAVE)){
                entrada = type+":"+sender+":"+msg;
            }*/
        }
        catch(IOException e){
            System.out.println("No se pudo recibir");
        }
        return entrada;
    }
}

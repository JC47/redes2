import java.net.*;
import java.io.*;

public class SEcoRTCPB {

    private static Socket cl;

    public static void main(String[] args) {
        try {
            ServerSocket s = new ServerSocket(1234);
            System.out.println("Esperando cliente");

            while (true) {
                cl = s.accept();
                System.out.println("Conexión aceptada desde " + cl.getInetAddress() + ": " + cl.getPort());

                PrintWriter pw = new PrintWriter(new OutputStreamWriter(cl.getOutputStream()));
                BufferedReader br = new BufferedReader(new InputStreamReader(cl.getInputStream()));
                
                try {
                    String msg1 = "Hola Cliente";
                    pw.println(msg1);
                    pw.flush();
                } catch (Exception e) {
                    //TODO: handle exception
                }

                try {
                    System.out.println("Respuesta del cliente: " + br.readLine());
                    pw.close();
                } catch (Exception e) {
                    //TODO: handle exception
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                cl.close();
            } catch (Exception e) {
            }
        }
    }
}